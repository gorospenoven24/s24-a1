//  number 2
db.users.find({
	 $or: [{ 
	 	"firstName":  {$regex:"S" ,$options: "$i"} 
	 	},
	 	{ "lastName":  {$regex:"D" ,$options: "$i"}  
	 }
         ]});

// number 3
db.users.find({
		$and: [
		{
		"age":{ $gt: 70 }
		}]
	});

// number 4
db.users.find({
	 $and: [{ 
	 	"firstName":  {$regex:"E" ,$options: "$i"} 
	 	},
	 	{ "age":  {$lte: 30}  
	 }
         ]});

